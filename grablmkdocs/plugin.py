from mkdocs.plugins import BasePlugin

from .link import ExternalLink

try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin


class GrablMkdocsPlugin(BasePlugin):
    def on_nav(self, nav, config):
        """Add links to external resources to navigation

        These resources, like API docs and test reports are built by
        jobs grabl's CI pipeline and deployed to grabl project pages,
        so available under `/grabl/` path, e.g.
        https://grabl.gitlab.io/grabl/api/

        MkDocs doesn't support external links in navigation yet:
        https://github.com/mkdocs/mkdocs/issues/989
        it has to be done using a plugin.
        """
        resources = [
            # title, url
            (u'API', u'api/'),
            (u'Test Report', u'reports/tests/test/'),
            (u'Coverage Report', u'reports/clover/html/'),
        ]

        for title, url in resources:
            # URLs can be relative to project pages URL
            url = urljoin(config[u'extra'][u'grabl_prj_url'], url)
            nav.nav_items.append(ExternalLink(title, url))
