class ExternalLink(object):
    def __init__(self, title, url):
        self.title, self.url = title, url

    def __repr__(self):
        return u'{}({!r}, {!r})'.format(
            self.__class__.__name__, self.title, self.url)
