[![pipeline status](https://gitlab.com/grabl/grabl.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/grabl/grabl.gitlab.io/commits/master)
# about

project page and documentation for grabl

https://grabl.gitlab.io/

## Building locally

To work with this project locally, you'll have to follow these steps:

1. Fork, clone or download this project
1. Get MkDocs using one of the following options:
   - [install MkDocs][mkdocs-inst],
     [Material for MkDocs][material-inst], [Pygments][pygments-inst],
     [PyMdown Extensions][pymdownext-inst]
   - or just [use docker][use-docker]:
     `docker pull squidfunk/mkdocs-material`
1. Preview your project: `./go serve`, your site can be accessed under
   `localhost:8000` or the port exposed by docker
1. Add content
1. Generate the website: `./go build` (optional)

Read more at [MkDocs][] and [Material for MkDocs][material]
documentation.


[mkdocs]: http://www.mkdocs.org/
[material]: https://squidfunk.github.io/mkdocs-material/
[mkdocs-inst]: http://www.mkdocs.org/#installation
[material-inst]: https://squidfunk.github.io/mkdocs-material/getting-started/#installation
[pygments-inst]: http://pygments.org/download/
[pymdownext-inst]: https://facelessuser.github.io/pymdown-extensions/installation/
[use-docker]: https://squidfunk.github.io/mkdocs-material/getting-started/#alternative-using-docker
