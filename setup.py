from setuptools import setup, find_packages


setup(
    name='grablmkdocs',
    version='0.0.0',
    description='Add external resources to grabl docs navigation',
    install_requires=['mkdocs>=0.17'],
    packages=find_packages(exclude=['*.tests']),
    entry_points={
        'mkdocs.plugins': [
            'grabl = grablmkdocs.plugin:GrablMkdocsPlugin',
        ]
    }
)
