# What is grabl

[Gradle] + [(OpenEdge) ABL][OpenEdge] = **grabl** (powered-by
[Riverside-Software PCT][PCT])

Grabl is a plugin for [Gradle] providing language support for
[OpenEdge ABL][OpenEdge].  It provides gradle tasks to compile ABL code
and run unit tests using ABLUnit.  All the hard work is done by [PCT],
thanks to gradle's fantastic integration with [Ant].

# How to use it

[Grabl] is hosted on the [Gradle Plugin Portal][grportal-grabl] so you
can use it by just adding this to your `build.gradle`:

``` groovy
plugins {
  id "io.gitlab.grabl.grabl" version "0.1.0"
}
```

# What does it do

 - adds [PCT] tasks and types to your project
 - adds tasks that integrate [PCT] with [Gradle] lifecycle tasks

# How does it do it

It modifies the [Gradle] project model in the following way:

 - adds new configuration _pct_
 - adds a repository where PCT can be downloaded from; this is
   temporary, we hope [PCT] can be published to Maven Central or
   JCenter in the future
 - adds a dependency on [PCT] 207
 - adds a dependency on Google gson 2.8.0 which is required by PCT
   ABLUnit task
 - loads PCT Ant tasks and types into AntBuilder using loader ref _pct_
 - creates native [Gradle] tasks

## Contributing ##

Want to suggest a feature or report a bug? Head to
[issue tracker][gragl-issues].

The source code is available on [GitLab][grabl-vcs].

Code contributions are very welcome, please check out
[hacking](hacking/) notes.

## License ##

grabl is free and open-source software licensed under the
[Apache License 2.0](https://gitlab.com/grabl/grabl/blob/master/LICENSE)


[Gradle]: https://gradle.org/
[OpenEdge]: https://www.progress.com/openedge
[grabl]: https://grabl.gitlab.io/
[grabl-vcs]: https://gitlab.com/grabl/grabl
[gragl-issues]: https://gitlab.com/grabl/grabl/issues
[PCT]: https://github.com/Riverside-Software/pct
[Ant]: http://ant.apache.org/
[grportal-grabl]: https://plugins.gradle.org/plugin/io.gitlab.grabl.grabl
