# Creating Custom Compile Tasks

The purpose of this guide is to show you how to setup a project that
configures compilation in a completely custom manner. We will use
*grabl-base* plugin, and therefore avoid loading any conventions and
instead create completely custom compilation tasks.

Make sure to check out [Gradle]'s
[Writing Custom Gradle Tasks][guide-wgt] guide to get a general idea of
how to implement custom tasks.

This guide builds on the [Creating a New ABL Project][guide-canap]
guide and it is assumed that you already have a basic `build.gradle`
file as described there.

The code resulting from following this guide can be found
[here][grabl-samples-custom-compile-tasks].

## Configuring to use the base plugin

Assuming you started with a [simple project][guide-canap] you already
have a `plugins` block in your `build.gradle` file that configures the
project to use the [grabl plugin][grabl-plugin]. In this case we'll
want to change this to [grabl-base plugin][grabl-base-plugin], to do
that simply change the name in your `plugins` block:

``` groovy
plugins {
  // use `grabl-base` to skip conventions and default tasks
  id "io.gitlab.grabl.grabl-base" version "0.1.0"
}
```

Using the *grabl-base* plugin means that the `compileAbl` task won't be
created for us, so we'll need to create our own.

## Creating tasks

First we assign the `CompileAblTask` to extension properties so that we
don't have to use its fully qualified name every time:

``` groovy
ext.CompileAblTask = io.gitlab.grabl.CompileAblTask
```

Now we can write our tasks. It doesn't matter what the task is called,
just make sure its type is `CompileAblTask`. In the task configuration
we specify the source files, we can specify additional options to pass
to the compiler (see documentation of [PCTCompile] to see what's
available), and finally we specify destination directory.

``` groovy
task compileAblSet01(type: CompileAblTask) {
  source 'src/main/abl/cctsample/main.p'

  // pass options to PCTCompile, see
  // https://github.com/Riverside-Software/pct/wiki/PCTCompile
  compileArgs.keepXref = true
  compileArgs.debugListing = true

  destinationDir = file("${abl.rcodeDir}/cctsample")
}
```

Multiple sources can be specified as an array.

``` groovy
task compileAblSet02(type: CompileAblTask) {
  source([
    'src/main/abl/cctsample/mod2/mod2.p',
    'src/main/abl/cctsample/mod2/mod2sibling1.p',
  ])

  destinationDir = file("${abl.rcodeDir}/cctsample/mod2")
}
```

Note that generally though, it is better to specify sources as a
directory, and an optional set of include/exclude patterns.

The reason is that when specifying path to a file the compiler doesn't
know what part of that path is a component of propath and which is the
program's package, so it strips the entire path, so e.g.
`src/main/abl/cctsample/mod2/mod2.p` becomes just `mod2.r`, which is
why the examples above specify the package as part of `destinationDir`.

Specifying a directory that is a component of propath as `source` and
using patterns to limit which files are used avoids those issues.

``` groovy
task compileAblSet03(type: CompileAblTask) {
  source 'src/main/abl'
  include 'cctsample/mod1.p', 'cctsample/mod3.p'
  include 'cctsample/mod4/*.p'

  // TODO: bug in CompileAblTask.groovy#L69, shouldn't be necessary
  destinationDir = abl.rcodeDir
}
```

## Hooking into a lifecycle task

Obviously we don't want to have to call out all those tasks on command
line every time to compile all sources. To avoid that we can hook into
a lifecycle task like **build** so that it triggers compilation of all
sources as necessary.

First we create a placeholder task that depends on all compilation
tasks. We use [tasks][grdsl-tasks] object provided by [Gradle] to find
all tasks starting with our chosen name.

``` groovy
// create a single placeholder task that depends on all other
// compilation tasks
task compileAbl
compileAbl.dependsOn {
  tasks.findAll { task -> task.name.startsWith('compileAblSet') }
}
```

Finally we make **build** depend on our placeholder compilation task,
and make it the default task.

``` groovy
build.dependsOn compileAbl
defaultTasks 'build'
```

## Build

That's it! Simply running `./gradlew` will now build the project and
compile all sources.

## Next Steps

You can inspect the full source that is the result of following this
guide in [this repository][grabl-samples-custom-compile-tasks] -
specifically have a look at the `build.gradle` file.

Learn more about [Gradle][] [build scripts][grdoc-bldscript] and
[tasks][grdoc-tasks].

Check docs of [PCTCompile] task to see what `compileArgs` you can set.

Have a look at [other guides](./) here to see what else is possible.


[Gradle]: https://gradle.org/
[grabl]: https://grabl.gitlab.io/
[grabl-plugin]: https://plugins.gradle.org/plugin/io.gitlab.grabl.grabl
[grabl-base-plugin]: https://plugins.gradle.org/plugin/io.gitlab.grabl.grabl-base
[PCT]: https://github.com/Riverside-Software/pct
[PCTCompile]: https://github.com/Riverside-Software/pct/wiki/PCTCompile
[grdoc-tasks]: https://docs.gradle.org/current/userguide/more_about_tasks.html
[grdoc-bldscript]: https://docs.gradle.org/current/userguide/writing_build_scripts.html
[grdsl-tasks]: https://docs.gradle.org/current/dsl/org.gradle.api.Project.html#org.gradle.api.Project:tasks
[guide-canap]: ./creating-a-new-abl-project/
[guide-wgt]: https://guides.gradle.org/writing-gradle-tasks/
[grabl-samples-custom-compile-tasks]: https://gitlab.com/grabl/grabl-samples/tree/master/custom-compile-tasks
