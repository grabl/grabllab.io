# Configuring Custom Paths

The purpose of this guide is to show you how to customise various paths
used in compilation task from [grabl]. It builds on the
[Creating a New ABL Project][guide-canap] guide so have a look there if
you're only getting started with [Gradle] or [grabl].

It is assumed that you already have a basic `build.gradle` file as
described in the [new project][guide-canap] guide, up to at least the
[Configure to use the plugin][guide-canap-ctutp] section.

The code resulting from following this guide can be found
[here][grabl-samples-custom-paths].


## Configure the source location

You can adjust where the compile task looks for source files easily.

The first thing you may need to do is to adjust the `PROPATH` where OpenEdge
will look for sources (include files, classes, etc.). If you're always
running in an enironment where `PROPATH` is already set via an env variable,
you can skip this. In order to set the `PROPATH` so it includes `src/`
directory add the following to your `build.gradle`:

```groovy
abl {
  // PROPATH value for OpenEdge, should include sources
  propath = files('src')
}
```

Next tell the compile task where to look for sources. Add the following
snippet to your `build.gradle`:

```groovy
compileAbl {
  // look for sources in `src` directory, recursively, include files
  // with `.p` extension
  source('src')
  include('**/*.p')
}
```

## Configure the output location

By default [grabl] follows the [Gradle] conventions and puts compiled r-code
in `${buildDir}/rcode` (by default `build/rcode/` in project dir). You can
override that easily by adding the following to your `build.gradle`:

```groovy
abl {
  // target dir for compiled r-code
  rcodeDir = 'newRcode'
}
```

!!! bug
    Note due to a [known bug](https://gitlab.com/grabl/grabl/issues/19) in
    [grabl] adding this is also necessary:

    ```groovy
    compileAbl {
      // TODO: bug in CompileAblTask.groovy#L69, shouldn't be necessary
      destinationDir = abl.rcodeDir
    }
    ```

Finally, if your configured r-code directory is outside of Gradle's
`${buildDir}` (`build/` by default), you may want to tell the **clean** task
to also delete that directory so that `./gradlew clean` does the right thing:

```groovy
clean {
  // configure clean task to remove our comilation artifacts,
  // this is only necessary because they're outside of `${buildDir}`
  delete += file(abl.rcodeDir)
}
```

## Build

That's it! Gradle should now pick up your code from the correct location and
[PCT] will be automatically configured to output the r-code where you want it.

## Next Steps

You can inspect the full source that is the result of following this guide
in [this repository][grabl-samples-custom-paths] - specifically have a
look at the `build.gradle` file.

If your project requires further customisation, even ignoring any conventions
in [grabl], have a look at [other guides](./) here.


[Gradle]: https://gradle.org/
[grabl]: https://grabl.gitlab.io/
[PCT]: https://github.com/Riverside-Software/pct
[guide-canap]: ./creating-a-new-abl-project/
[guide-canap-ctutp]: ./creating-a-new-abl-project/#configure-to-use-the-plugin
[grabl-samples-custom-paths]: https://gitlab.com/grabl/grabl-samples/tree/master/custom-paths
