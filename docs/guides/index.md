# Home

Guides to help you use [grabl] in your projects. Whether you're getting
started or trying to learn how to support more complex scenarios, there
should be something here for you.

You can find sample projects and code corresponding to every guide here
in [grabl-samples] repository.

# Available guides

## [Creating a New ABL Project](creating-a-new-abl-project/)

Learn how to create a very simple project, that follows
[gradle]/[grabl] conventions (that is sources in `src/main/abl`, tests
in `src/test/abl`, etc.) and thus requires almost no configuration.

## [Configuring Custom Paths](configuring-custom-paths/)

Learn how to configure a project that follows most conventions but
customises where the source files are and where to put the compiled
r-code.

## [Using PCT Tasks](using-pct-tasks/)

See how to use types and tasks provided by [PCT] to interact with
[OpenEdge] from [Gradle]. It implements a task that creates an
[OpenEdge] database and loads the schema.

## [Creating Custom Compile Tasks](creating-custom-compile-tasks/)

Setup a project that configures compilation in a completely custom
manner. Use *grabl-base* plugin, and therefore avoid loading any
conventions and instead create your own compilation tasks.


[gradle]: https://gradle.org/
[grabl]: https://grabl.gitlab.io/
[grabl-samples]: https://gitlab.com/grabl/grabl-samples
[PCT]: https://github.com/Riverside-Software/pct
[OpenEdge]: https://www.progress.com/openedge
