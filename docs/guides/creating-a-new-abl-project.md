# Creating a New ABL Project

The purpose of this guide is to show you how to get started with an ABL based
project and use [Gradle] and [grabl].

The code resulting from following this guide can be found
[here][grabl-samples-simple].

## Create a project

First you'll need a basic Gradle project. If you're getting started with
Gradle it might be worth reading through [Creating New Gradle Builds] guide.
It's worth following it to get a feel for how you work with Gradle, etc.

!!! tldr "TL;DR"
    ```sh
    $ mkdir simple-abl-demo
    $ cd simple-abl-demo
    $ touch build.gradle settings.gradle
    $ gradle wrapper
    ```

## Configure to use the plugin

Configuration of the build happens in the `build.gradle` file. This file is in
[Groovy] format and you can use a [DSL that Gradle exposes][gradle-docs-dsl]
to configure the build and/or script additional actions.

Start by opening up `build.gradle` in your favourite editor and add the
following snippets at the top.

This instructs Gradle to use the [grabl] plugin:

```groovy
plugins {
  id "io.gitlab.grabl.grabl" version "0.1.0"
}
```

The following tells Gradle to look for any
[dependencies of grabl](/#how-does-it-do-it) and your build script in
[JCenter repository][gradle-docs-man-dep-man-jcenter]. See
[docs on dependency management in Gradle][gradle-docs-man-dep-man] and
specifically the
[section on repositories][gradle-docs-man-dep-man-repos].

```groovy
repositories {
  jcenter()
}
```

Apply the [base plugin] which adds standard lifecycle tasks like **build**.

```groovy
apply plugin: 'base'
```

## Configure the compilation

In the future none of this will be necessary, but in the meantime while the
features get implemented, you need to add a bit of configuration to
`build.gradle`. Insert the following at the bottom of the file.

This snippet hooks the compile task into **build** task and configures that
as a default task so it gets executed automatically when you just type
`./gradlew` in your source:

```groovy
build.dependsOn compileAbl
defaultTasks 'build'
```

The following configures the compilation task so it takes the sources from
`src/main/abl` directory:

```groovy

compileAbl {
  source('src/main/abl')

  // TODO: bug in CompileAblTask.groovy#L69, shouldn't be necessary
  destinationDir = abl.rcodeDir
}
```

## Build

That's it! You should now be able to run `./gradlew` in the source directory
and see something like:

```sh
$ ./gradlew

BUILD SUCCESSFUL in 2s
1 actionable task: 1 executed
```

## Next Steps

You can inspect the full source that is the result of following this guide
in [this repository][grabl-samples-simple] - specifically have a look
at the `build.gradle` file.

If you're working with an existing project with established conventions which
do not match the defaults in [grabl], have a look at
[other guides](./) here.


[Gradle]: https://gradle.org/
[grabl]: https://grabl.gitlab.io/
[Groovy]: http://groovy-lang.org/
[Creating New Gradle Builds]: https://guides.gradle.org/creating-new-gradle-builds/
[gradle-docs-dsl]: https://docs.gradle.org/current/dsl/
[gradle-docs-man-dep-man]: https://docs.gradle.org/4.4.1/userguide/dependency_management.html
[gradle-docs-man-dep-man-repos]: https://docs.gradle.org/4.4.1/userguide/dependency_management.html#sec:repositories
[gradle-docs-man-dep-man-jcenter]: https://docs.gradle.org/4.4.1/userguide/dependency_management.html#sub:maven_jcenter
[base plugin]: https://docs.gradle.org/current/userguide/standard_plugins.html#sec:base_plugins
[grabl-samples-simple]: https://gitlab.com/grabl/grabl-samples/tree/master/simple
