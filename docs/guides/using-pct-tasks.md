# Using PCT Tasks

The purpose of this guide is to show you how to use types and tasks
provided by [PCT] to interact with [OpenEdge] from [Gradle].

A quick clarification of the terminology here, both [Gradle] and [Ant]
which [PCT] is built on, use the term *task* but refer to different
things. Here is a short translation table

| Ant    | Gradle             |
|--------|--------------------|
| type   | class / method     |
| task   | task type / action |
| target | task               |

You can check out [Gradle]'s [Using Ant From Gradle][grdoc-ant] doc for
more info and examples on integration with [Ant].

This guide builds on the [Creating a New ABL Project][guide-canap]
guide and it is assumed that you already have a basic `build.gradle`
file as described there.

The code resulting from following this guide can be found
[here][grabl-samples-pct-tasks].

## Create a new task

We'll start by creating a basic structure for a task, so that we can
then keep adding to it.

``` groovy
task createTestDb {
  group = 'utility'
  description = 'create a test OpenEdge database'

  doLast {
  }
}
```

Setting the `group` and `description` properties makes our new task
appear in `./gradlew tasks` output.

## Invoking PCT Tasks

Invoking [PCT] tasks as part of your [Gradle] task's actions is as
simple as calling a method on an `ant` object. E.g. we can invoke
[PCTVersion] task like so:

``` groovy
ant.PCTVersion()
```

## Creating a database

[PCT] provides a handy [PCTCreateBase] task for this. We only need to
make sure the target directory exists.

``` groovy
def dbDir = "${buildDir}/db"
file(dbDir).mkdirs()
ant.PCTCreateBase(destDir: dbDir, dbName: 'testfoo',
                  largeFiles: true)
```

As you can see we can set [PCT] task attributes by passing Groovy's
[Map parameters][grv-map-params] to the method invocation.

## Populating the schema

Normally [PCTCreateBase] provides a shortcut for this - the
`schemaFile` attribute, but since we want to explain using [PCT] tasks
we can do it the more verbose way.

We load the schema by calling provided [PCTLoadSchema] task. This task
requires the `srcFile` attribute to be set which we can do by passing
an argument, but it also needs to know which database to connect to.

This is specified in [Ant]'s XML as a nested element, i.e.
`<PCTLoadSchema><DBConnection /></PCTLoadSchema>`. In [Gradle] we do
this by passing a closure and configuring nested elements inside it.

``` groovy
ant.PCTLoadSchema(srcFile: 'testfoo.df') {
  DBConnection(dbDir: dbDir, dbName: 'testfoo', singleUser: true)
}
```

We used `DBConnection` here which is an alias for [PCTConnection] type.

??? tip "Reusing predefined connections"
    Note that if you're using the DB connection(s) in multiple places,
    in order to avoid having to repeat the parameters everywhere, you
    can define the connections once (perhaps in a separate task),
    giving them an ID:

    ``` groovy
    ant.DBConnection(id: 'foo', dbDir: "${buildDir}/db", dbName: 'foo')
    ```

    and refer to them elsewhere with just:

    ``` groovy
    ant.DBConnection(refid: 'foo')
    ```

## Build

That's it! This is how the final task looks like:

``` groovy
task createTestDb {
  group = 'utility'
  description = 'create a test OpenEdge database'

  doLast {
    ant.PCTVersion()

    // create a database named 'testfoo'
    def dbDir = "${buildDir}/db"
    file(dbDir).mkdirs()
    ant.PCTCreateBase(destDir: dbDir, dbName: 'testfoo',
                      largeFiles: true)

    // load schema into the newly created database
    ant.PCTLoadSchema(srcFile: 'testfoo.df') {
      DBConnection(dbDir: dbDir, dbName: 'testfoo', singleUser: true)
    }
  }
}
```

We can now invoke it using `./gradlew createTestDb` and we should see
our newly created database in `build/db/` directory.

## Next Steps

You can inspect the full source that is the result of following this
guide in [this repository][grabl-samples-pct-tasks] - specifically have
a look at the `build.gradle` file.

Explore the [API][PCT-API] provided bt [PCT], all those tasks and types
can be used in your [Gradle] tasks.

Have a look at [other guides](./) here to see what else is possible.


[Gradle]: https://gradle.org/
[grabl]: https://grabl.gitlab.io/
[PCT]: https://github.com/Riverside-Software/pct
[PCT-API]: https://github.com/Riverside-Software/pct/wiki
[PCTVersion]: https://github.com/Riverside-Software/pct/wiki/PCTVersion
[PCTCreateBase]: https://github.com/Riverside-Software/pct/wiki/PCTCreateBase
[PCTLoadSchema]: https://github.com/Riverside-Software/pct/wiki/PCTLoadSchema
[PCTConnection]: https://github.com/Riverside-Software/pct/wiki/PCTConnection
[Ant]: http://ant.apache.org/
[OpenEdge]: https://www.progress.com/openedge
[guide-canap]: ./creating-a-new-abl-project/
[grdoc-ant]: https://docs.gradle.org/4.5/userguide/ant.html
[grv-map-params]: http://groovy-lang.org/objectorientation.html#_named_arguments
[grabl-samples-pct-tasks]: https://gitlab.com/grabl/grabl-samples/tree/master/pct-tasks
