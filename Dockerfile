FROM squidfunk/mkdocs-material:latest

LABEL Name=grabl.gitlab.io Version=0.0.0

COPY grablmkdocs /src/grablmkdocs
COPY setup.py /src/
RUN pip install -e /src/
